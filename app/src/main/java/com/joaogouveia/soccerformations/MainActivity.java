package com.joaogouveia.soccerformations;

import android.support.v4.view.animation.FastOutLinearInInterpolator;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;

import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Scene;
import com.transitionseverywhere.TransitionManager;
import com.transitionseverywhere.TransitionSet;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_main)
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    @ViewById
    Spinner spinner;

    @ViewById
    RelativeLayout field;

    private Scene scene442;
    private Scene scene433;
    private Scene scene343;
    private Scene scene352;
    private Scene scene532;

    @AfterViews
    void afterViews() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.formations_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);


        scene442 = Scene.getSceneForLayout(field, R.layout.formation_4_4_2, this);
        scene433 = Scene.getSceneForLayout(field, R.layout.formation_4_3_3, this);
        scene343 = Scene.getSceneForLayout(field, R.layout.formation_3_4_3, this);
        scene352 = Scene.getSceneForLayout(field, R.layout.formation_3_5_2, this);
        scene532 = Scene.getSceneForLayout(field, R.layout.formation_5_3_2, this);
    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        TransitionSet set = new TransitionSet();
        set.addTransition(new ChangeBounds());
        set.setDuration(400);
        set.setInterpolator(new AccelerateDecelerateInterpolator());
        switch (i) {
            case 0:
                TransitionManager.go(scene442, set);
                break;
            case 1:
                TransitionManager.go(scene433, set);
                break;
            case 2:
                TransitionManager.go(scene343, set);
                break;
            case 3:
                TransitionManager.go(scene352, set);
                break;
            case 4:
                TransitionManager.go(scene532, set);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
