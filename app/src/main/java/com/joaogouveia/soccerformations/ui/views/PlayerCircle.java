package com.joaogouveia.soccerformations.ui.views;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.joaogouveia.soccerformations.R;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

/**
 * Created by joaogouveia on 15/02/16.
 */
@EViewGroup(R.layout.view_player_circle)
public class PlayerCircle extends LinearLayout {

    private String mText;

    @ViewById
    TextView text;

    public PlayerCircle(Context context) {
        super(context);
        getAttrs(context, null);
    }

    public PlayerCircle(Context context, AttributeSet attrs) {
        super(context, attrs);
        getAttrs(context, attrs);
    }

    public PlayerCircle(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttrs(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public PlayerCircle(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        getAttrs(context, attrs);
    }

    @AfterViews
    void afterViews() {
        applyAttrs();
    }

    private void getAttrs(Context context, AttributeSet attrs) {
        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.PlayerCircle);
        mText = values.getString(R.styleable.PlayerCircle_pc_text);
        values.recycle();
    }

    private void applyAttrs() {
        text.setText(mText);
    }
}
